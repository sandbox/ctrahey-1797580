<?php
/**
 * @file views_url_args.views.inc
 * Provides %1 - style wildcards in
 * Views filter criteria and title.
 * @author Chris Trahey <christrahey@gmail.com>
 */

/**
 * implements hook_views_pre_build()
 *
 * Alters view filter values to pass in view arguments.
 * In the Views UI, specify a path with wildcards, and
 * %1-style segment identifier as the 'value' of a filter.
 * This will replace the compared value with the corresponding URL segment
 * i.e. filter setup for price >= %1 with a path /price/% and request /price/100
 * will evaluate to price >= 100
 */
function views_url_args_views_pre_build(&$view) {
  if(!isset($view->filter)) {
    return;
  }
  $remove_filters = array();
  foreach($view->filter as $key=>$handler) {
    if(!property_exists($handler, 'value')) {
      continue;
    }
    if(is_array($handler->value)) {
      foreach($handler->value as $param=>$value_pattern) {
        views_url_args_process_arg($handler->value[$param], $view, $remove_filters, $key);
      }
    } else {
      views_url_args_process_arg($handler->value, $view, $remove_filters, $key);
    }
  }
  foreach($remove_filters as $key) {
    unset($view->filter[$key]);
  }
}

/**
 * process an argument, interpreting wildcards and filter-removal flags
 * @param &$value string (by reference) the configured pattern (i.e. '%1 *=any')
 * @param &$view view object being processed, used for accessing args
 * @param &$remove_filters array (reference) fitler with keys added to this array will be removed from the query
 * @param $filter_key mixed the key identifying this filter in the view's filter array
 */
function views_url_args_process_arg(&$value, &$view, &$remove_filters, $filter_key) {
  if(0 !== strpos($value, '%')) {
    return;
  }
  $arg_num = intval(ltrim($value, '%'));
  $value_candidate = $view->args[$arg_num - 1];
  // Remove the filter if a configured wildcard is passed
  if(strpos($value, '*=')) {
    $wildcard_pattern = '/\*=(.*)\b/';
    $matches = array();
    $wildcard_string = preg_match($wildcard_pattern, $value, $matches);
    if($value_candidate == $matches[1]) {
      $remove_filters[] = $filter_key;
    } 
  }
  $value = $value_candidate;
}


/**
 * implements hook_views_pre_view()
 *
 * View Title rewriting with URL-supplied arguments
 * In Views UI, specify a title for your view like: "Prices above %1"
 */
function views_url_args_views_pre_view(&$view) {
  $display = $view->display[$view->current_display];
  $title_pattern = $display->handler->options['title'];
  if(FALSE == strpos($title_pattern, '%')) {
    return;
  }
  $indeces = preg_match_all('/%([0-9]+)/', $title_pattern, $matches);
  $matches = array_unique($matches[1]);
  foreach($matches as $arg_num) {
    if($arg_num > count($view->args)) {
      continue;
    }
    $title_pattern = preg_replace('/%'.$arg_num.'\b/', $view->args[$arg_num - 1], $title_pattern);
  }
  $display->display_options['title'] = $display->handler->options["title"] = $title_pattern;
}